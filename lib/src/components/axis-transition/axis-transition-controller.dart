part of 'axis-transition.dart';

/// a [AxisTransitionController] allows you to control an [AxisTransitionView]
class AxisTransitionController {
  _AxisTransitionViewState? _globalKey;

  /// animates to the next child
  void next() => _globalKey!.next();

  /// animates to the previous child
  void previous() => _globalKey!.previous();

  /// the current child
  int get current => _globalKey?.current ?? 0;

  /// animates to the given child
  set current(int i) => _globalKey!.current = i;

  set _key(_AxisTransitionViewState key) => _globalKey = key;
}
