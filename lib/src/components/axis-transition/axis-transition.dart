import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

part 'axis-transition-controller.dart';

/// a high-level interface to perform smooth axis animations
class AxisTransitionView extends StatefulWidget {
  /// the children to be animated
  ///
  /// **Please note**: every child is required to have a unique
  /// [Key]. Otherwise the controller won't be able to guess whether to start
  /// an animation on [setState] or not.
  final List<Widget> children;

  /// the [AxisTransitionController] performing the animations
  final AxisTransitionController controller;

  /// the [SharedAxisTransitionType] to animate threw
  ///
  /// default to [SharedAxisTransitionType.horizontal]
  final SharedAxisTransitionType direction;

  /// the [Color] to be used for the background
  final Color backgroundColor;

  const AxisTransitionView(
      {Key? key,
      required this.children,
      required this.controller,
      this.direction = SharedAxisTransitionType.horizontal,
      this.backgroundColor = Colors.transparent})
      : super(key: key);
  @override
  _AxisTransitionViewState createState() => _AxisTransitionViewState();
}

class _AxisTransitionViewState extends State<AxisTransitionView> {
  bool _reverse = false;

  int _i = 0;

  @override
  void initState() {
    widget.controller._key = this;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PageTransitionSwitcher(
        duration: const Duration(milliseconds: 300),
        reverse: _reverse,
        transitionBuilder: (child, animation, secondaryAnimation) {
          return SharedAxisTransition(
              fillColor: widget.backgroundColor,
              child: child,
              animation: animation,
              secondaryAnimation: secondaryAnimation,
              transitionType: widget.direction);
        },
        layoutBuilder: PageTransitionSwitcher.defaultLayoutBuilder,
        child: widget.children[_i]);
  }

  void next() {
    setState(() {
      _reverse = false;
      _i++;
    });
  }

  void previous() {
    setState(() {
      _reverse = true;
      _i--;
    });
  }

  int get current => _i;

  set current(int i) {
    setState(() {
      _reverse = i < _i;
      _i = i;
    });
  }
}
