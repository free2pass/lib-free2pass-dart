/// the abstract [BaseApi] all application's APIs extend
abstract class BaseApi {}

/// an [Error] thrown in case the API is not initialized when performing a request
class F2PApiNotInitializedError extends Error {
  @override
  String toString() {
    return 'Error: F2PApi not initialized yet. Please run F2PApi.initialize() before any call of F2PApi.instance.';
  }
}
