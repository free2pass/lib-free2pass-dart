import 'dart:convert';

import 'package:http/http.dart';

import '../../free2pass_support.dart';

// TODO: clean file, remove unnecessary methods and deprecated authentication mechanism

/// The [ApiModel] contains the code for all http requests we send to our servers.
///
/// It handles authentication, debug output and errors.
/// Implementors must provide the getters
/// - [token]
/// - [isDebug]
/// - [getApiKey]
abstract class ApiModel {
  /// returns the JSON web token used for API authentication
  String? get token => null;

  /// tells the API whether to log debug output
  bool get isDebug;

  /// the secret API key used for authentication in initial requests
  String? get getApiKey => null;

  /// the personal signature key of the device used to sign every http request
  String? get signatureKey => null;

  /// performs a GET http request
  Future<ResponseModel<T>> doGetRequest<T>(
    /// the base URL of the API
    /// e.g. http://localhost:5000/
    String baseUrl,

    /// th endpoint to address
    /// e.g. devices/laptops/3182
    String endpoint, {

    /// parameters to be sent as GET parameters
    Map<String, dynamic>? params,

    /// additional headers to be sent
    Map<String, String>? headers,

    /// sign the request with the given signature data if not null
    String? signRequest,

    /// whether to use the JSON web token
    bool useToken = true,
  }) async {
    try {
      headers ??= {};

      var url = baseUrl + '/' + endpoint;

      if (useToken && token != null) {
        headers['Authorization'] = 'Bearer ' + token!;
        final signatureString = url + jsonEncode(params) + signatureKey!;
        headers['Signature'] =
            Crypto.getSha256Sum(signatureString.replaceAll(RegExp(r'\s'), ''));
      }
      if (signRequest != null && token == null) {
        final signatureString = params != null
            ? url + jsonEncode(params) + signRequest
            : url + signRequest;
        headers['Signature'] =
            Crypto.getSha256Sum(signatureString.replaceAll(RegExp(r'\s'), ''));
      }

      final baseUri = Uri.parse(url);

      var uri = Uri(
        scheme: baseUri.scheme,
        host: baseUri.host,
        path: baseUri.path,
        queryParameters: params,
      );

      if (isDebug) {
        print('GET-Request to $uri');
      }
      final response = await get(uri, headers: headers);
      if (response.statusCode >= 200 && response.statusCode < 300) {
        if (isDebug) {
          print(response.body);
        }
        return ResponseModel<T>.fromJson(json.decode(response.body));
      } else {
        print('Get-Request to ' +
            url +
            ' - body: ' +
            response.body +
            ' - params: ' +
            json.encode(params));
      }
      // ignore: empty_catches
    } catch (e) {}

    // call went wrong
    return ResponseModel<T>(
        null, Status(false, 'runtime_error', ErrorCodes.RuntimeError));
  }

  /// performs a PATCH http request
  Future<ResponseModel<T>> doPatchRequest<T>(
    /// the base URL of the API
    /// e.g. http://localhost:5000/
    String baseUrl,

    /// th endpoint to address
    /// e.g. devices/laptops/3182
    String endpoint, {

    /// parameters to be sent as PATCH parameters
    Map<String, dynamic>? params,

    /// additional headers to be sent
    Map<String, String>? headers,

    /// sign the request with the given signature data if not null
    String? signRequest,

    /// whether to use the JSON web token
    bool useToken = true,
  }) async {
    try {
      headers ??= {};

      var url = baseUrl + '/' + endpoint;

      if (useToken && token != null) {
        headers['Authorization'] = 'Bearer ' + token!;
        final signatureString = url + jsonEncode(params) + signatureKey!;
        headers['Signature'] =
            Crypto.getSha256Sum(signatureString.replaceAll(RegExp(r'\s'), ''));
      }
      if (signRequest != null && token == null) {
        final signatureString = params != null
            ? url + jsonEncode(params) + signRequest
            : url + signRequest;
        headers['Signature'] =
            Crypto.getSha256Sum(signatureString.replaceAll(RegExp(r'\s'), ''));
      }

      if (isDebug) {
        print('PATCH-Request to $url');
      }

      final uri = Uri.parse(url);

      final response = await patch(
        uri,
        body: jsonEncode(params),
        headers: headers,
      );
      if (response.statusCode >= 200 && response.statusCode < 300) {
        if (isDebug) {
          print(response.body);
        }
        return ResponseModel<T>.fromJson(json.decode(response.body));
      } else {
        print('Patch-Request to ' +
            url +
            ' - body: ' +
            response.body +
            ' - params: ' +
            json.encode(params));
      }
      // ignore: empty_catches
    } catch (e) {}

    // call went wrong
    return ResponseModel<T>(
        null, Status(false, 'runtime_error', ErrorCodes.RuntimeError));
  }

  /// performs a POST http request
  Future<ResponseModel<T>> doPostRequest<T>(
    /// the base URL of the API
    /// e.g. http://localhost:5000/
    String baseUrl,

    /// th endpoint to address
    /// e.g. devices/laptops/3182
    String endpoint, {

    /// parameters to be sent as request body
    Map<String, dynamic>? params,

    /// additional headers to be sent
    Map<String, String>? headers,

    /// sign the request with the given signature data if not null
    String? signRequest,

    /// whether to use the JSON web token
    bool useToken = true,
  }) async {
    try {
      if (headers == null) {
        headers = {'Content-Type': 'application/json'};
      } else {
        headers['Content-Type'] = 'application/json';
      }

      String paramsString = json.encode(params);

      if (token == null && getApiKey != null) {
        final String checkSum = Crypto.getSha256Sum(
            paramsString.replaceAll(RegExp(r'\s'), '') + getApiKey!);
        params!['key'] = checkSum;
        paramsString = json.encode(params).replaceAll(RegExp(r'\s'), '');
      }

      final url = baseUrl + '/' + endpoint;

      if (useToken && token != null) {
        headers['Authorization'] = 'Bearer ' + token!;
        final signatureString = url + paramsString + signatureKey!;
        headers['Signature'] =
            Crypto.getSha256Sum(signatureString.replaceAll(RegExp(r'\s'), ''));
      }
      if (signRequest != null && token == null) {
        final signatureString = params != null
            ? url + jsonEncode(params) + signRequest
            : url + signRequest;
        headers['Signature'] =
            Crypto.getSha256Sum(signatureString.replaceAll(RegExp(r'\s'), ''));
      }

      if (isDebug) {
        print('POST-Request to $url');
      }
      final response = await post(
        Uri.parse(url),
        body: paramsString,
        headers: headers,
      );

      if (response.statusCode >= 200 && response.statusCode < 300) {
        if (isDebug) {
          print(response.body);
        }
        return ResponseModel<T>.fromJson(json.decode(response.body));
      } else {
        print('Post-Request to ' +
            url +
            ' - body: ' +
            response.body +
            ' - params: ' +
            json.encode(params));
      }
      // ignore: empty_catches
    } catch (e) {}

    // call went wrong
    return ResponseModel<T>(
        null, Status(false, 'runtime_error', ErrorCodes.RuntimeError));
  }

  /// performs a PUT http request
  Future<ResponseModel<T>> doPutRequest<T>(
    /// the base URL of the API
    /// e.g. http://localhost:5000/
    String baseUrl,

    /// th endpoint to address
    /// e.g. devices/laptops/3182
    String endpoint, {

    /// parameters to be sent as request body
    Map<String, dynamic>? params,

    /// additional headers to be sent
    Map<String, String>? headers,

    /// whether to use the JSON web token
    bool useToken = true,
  }) async {
    try {
      if (headers == null) {
        headers = {'Content-Type': 'application/json'};
      } else {
        headers['Content-Type'] = 'application/json';
      }

      final url = baseUrl + '/' + endpoint;

      if (useToken && token != null) {
        headers['Authorization'] = 'Bearer ' + token!;
        final signatureString = url + jsonEncode(params) + signatureKey!;
        headers['Signature'] =
            Crypto.getSha256Sum(signatureString.replaceAll(RegExp(r'\s'), ''));
      }

      if (isDebug) {
        print('PUT-Request to $url');
      }
      final response = await put(Uri.parse(url),
          body: json.encode(params), headers: headers);

      if (response.statusCode >= 200 && response.statusCode < 300) {
        if (isDebug) {
          print(response.body);
        }
        return ResponseModel<T>.fromJson(json.decode(response.body));
      } else {
        // Unexpected Error
        print('Put-Request to ' +
            url +
            ' - body: ' +
            response.body +
            ' - params: ' +
            json.encode(params));
      }
      // ignore: empty_catches
    } catch (e) {}

    // call went wrong
    return ResponseModel<T>(
        null, Status(false, 'runtime_error', ErrorCodes.RuntimeError));
  }

  /// performs a DELETE http request
  Future<ResponseModel<T>> doDeleteRequest<T>(
    /// the base URL of the API
    /// e.g. http://localhost:5000/
    String baseUrl,

    /// th endpoint to address
    /// e.g. devices/laptops/3182
    String endpoint, {

    /// parameters to be sent as DELETE parameters
    Map<String, String>? params,

    /// additional headers to be sent
    Map<String, String>? headers,

    /// whether to use the JSON web token
    bool useToken = true,
  }) async {
    try {
      headers ??= {};

      String url = baseUrl + '/' + endpoint;

      if (useToken && token != null) {
        headers['Authorization'] = 'Bearer ' + token!;
        final signatureString = url + jsonEncode(params) + signatureKey!;
        headers['Signature'] =
            Crypto.getSha256Sum(signatureString.replaceAll(RegExp(r'\s'), ''));
      }
      if (params != null) {
        var isFirst = true;
        params.forEach((key, val) {
          url += isFirst ? '?' : '&';
          url += key + '=' + val;
          if (isFirst) {
            isFirst = false;
          }
        });
      }

      if (isDebug) {
        print('DELETE-Request to $url');
      }
      final response = await delete(Uri.parse(url), headers: headers);
      if (response.statusCode >= 200 && response.statusCode < 300) {
        if (isDebug) {
          print(response.body);
        }
        return ResponseModel<T>.fromJson(json.decode(response.body));
      } else {
        print('Delete-Request to ' +
            url +
            ' - body: ' +
            response.body +
            ' - params: ' +
            json.encode(params));
      }
      // ignore: empty_catches
    } catch (e) {}

    // call went wrong
    return ResponseModel<T>(
        null, Status(false, 'runtime_error', ErrorCodes.RuntimeError));
  }
}
