import 'package:camera_camera/camera_camera.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/material.dart';

/// a dialog allowing to take a photo
///
/// popping a [FilePickerCross] containing the corresponding file
class CameraDialog extends StatelessWidget {
  /// the [CameraSide] to be used
  ///
  /// [CameraSide.back] by default
  final CameraSide side;

  const CameraDialog({Key? key, this.side = CameraSide.back}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black,
        alignment: Alignment.center,
        child: AspectRatio(
          aspectRatio: 9 / 16, //TODO: workaround hardcoded aspect ratio
          child: CameraCamera(
            resolutionPreset: ResolutionPreset.veryHigh,
            onFile: (file) {
              final image = FilePickerCross(
                file.readAsBytesSync(),
              );
              Navigator.of(context).pop(image);
            },
            cameraSide: side,
          ),
        ),
      ),
    );
  }
}
