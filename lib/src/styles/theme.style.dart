import 'package:flutter/material.dart';

import 'colors.style.dart';

/// the default text style to be used in the application
const defaultTextStyle = TextStyle(
  fontFamily: _fontName,
  color: gray,
  fontWeight: FontWeight.w500,
);

/// the [defaultTextStyle] with blue font color
final blueTextStyle = defaultTextStyle.copyWith(color: blue);

/// the [defaultTextStyle] with bold font
final boldTextStyle = defaultTextStyle.copyWith(fontWeight: FontWeight.bold);

/// the [blueTextStyle] with bold font
final blueBoldTextStyle = blueTextStyle.copyWith(fontWeight: FontWeight.bold);

/// the theme to be used for big icons
const bigIconTheme = IconThemeData(size: 32);

final BottomNavigationBarThemeData _bottomNavigationBarTheme =
    BottomNavigationBarThemeData(
  unselectedIconTheme: bigIconTheme,
  selectedIconTheme: bigIconTheme.copyWith(color: blue),
  unselectedLabelStyle: blueTextStyle,
  selectedLabelStyle: blueBoldTextStyle,
  elevation: 0,
);

final _appBarTheme = AppBarTheme(
  foregroundColor: Colors.white,
  centerTitle: true,
  titleTextStyle: TextStyle(color: Colors.white),
  iconTheme: IconThemeData(color: Colors.white),
);

final _floatingActionButtonTheme = FloatingActionButtonThemeData(
    backgroundColor: green, foregroundColor: Colors.white);

final _elevatedButtonTheme = ElevatedButtonThemeData(
  style: ElevatedButton.styleFrom(
    primary: green,
  ),
);

final _outlinedButtonTheme = OutlinedButtonThemeData(
  style: OutlinedButton.styleFrom(
    primary: blue,
  ),
);

final _textButtonTheme = TextButtonThemeData(
  style: TextButton.styleFrom(
    primary: blue,
  ),
);

const _fontName = 'Quicksand';

/// the light [ThemeData] for the application
final lightTheme = ThemeData(
  brightness: Brightness.light,
  colorScheme: ColorScheme.light(primary: green, secondary: blue),
  primaryColor: green,
  fontFamily: _fontName,
  floatingActionButtonTheme: _floatingActionButtonTheme,
  bottomNavigationBarTheme: _bottomNavigationBarTheme,
  appBarTheme: _appBarTheme,
  elevatedButtonTheme: _elevatedButtonTheme,
  outlinedButtonTheme: _outlinedButtonTheme,
  textButtonTheme: _textButtonTheme,
);

/// the dark [ThemeData] for the application
final darkTheme = ThemeData(
  brightness: Brightness.dark,
  colorScheme: ColorScheme.dark(primary: green, secondary: blue),
  primaryColor: green,
  fontFamily: _fontName,
  bottomNavigationBarTheme: _bottomNavigationBarTheme,
  floatingActionButtonTheme: _floatingActionButtonTheme,
  appBarTheme: _appBarTheme,
  elevatedButtonTheme: _elevatedButtonTheme,
  outlinedButtonTheme: _outlinedButtonTheme,
  textButtonTheme: _textButtonTheme,
);
