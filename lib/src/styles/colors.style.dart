import 'package:flutter/material.dart';

/// the main Green color
const green = Color(0xff00c58d);
/// the Red alert color
const red = Color(0xffaa003c);
/// the Blue text color
const blue = Color(0xff3d5ba0);
/// the Gray text color
const gray = Color(0xff3c3c3b);
/// the semi-transparent background color
const introBackground = Color(0x66dae3ff); // TODO: support dark theme
