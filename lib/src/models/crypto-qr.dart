/// free2**pass**' Crypto QR implementation
///
/// Crypto QRs consist of an ID and a cryptographic key.
/// This data can be used for en- and decryption later
class CryptoQrData {
  /// the ID provided in the QR data
  final String id;

  /// the cryptographic key provided in the QR data
  final String key;

  CryptoQrData._({required this.id, required this.key});

  /// parses a crypto QR code form a raw [String]
  factory CryptoQrData.fromRaw(String data) {
    // removing deeplink part
    data = data.replaceAll(
      RegExp(r'https:\/\/go\.free2pass\.de\/#\/\w+\/'),
      '',
    );
    // to not use `data.split(';')` as the key might contain a `;`
    final splitAt = data.indexOf(';');
    final testId = data.substring(0, splitAt);
    final publicKey = data.substring(splitAt + 1);
    return CryptoQrData._(id: testId, key: publicKey);
  }

  @override
  String toString() {
    return id + ';' + key;
  }

  /// Crypto QR code regex
  static final regex = RegExp(
      r'^(https:\/\/go\.free2pass\.de\/#\/\w+\/)?[a-zA-Z0-9\-\+]+;[-A-Za-z0-9+\/=]+$');
}
