/// calculates the bottom padding for our app's main screens
double getBottomPadding(double height) {
  return 56 + height / 24;
}

/// calculates the top padding for our app's main screens
double getTopPadding(double height) {
  return height / 24;
}
