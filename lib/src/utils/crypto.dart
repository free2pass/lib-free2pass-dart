import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:dbcrypt/dbcrypt.dart';
import 'package:encrypt/encrypt.dart';
import 'package:openpgp/openpgp.dart';
import 'package:pointycastle/export.dart';
import 'package:rsa_encrypt/rsa_encrypt.dart';

import '../../free2pass_support.dart';

/// the library performing all cryptography for our apps
///
/// **For AES encryption, some information is required for the en- and decryption:**
/// - Of course, we always require two peaces of data:
///   - cipher - the plain text cipher we use for encryption
///   - cryptoData - our encrypted data
///     - the data is base64-encoded
///     - the decoded data byte list consists of two part:
///       - bytes 0 ... 16: the AES initialisation vector
///       - bytes 16 ... n: the encrypted string
///   - For crypto, you require the following settings:
///     - mode: CBC
///     - padding: PKCS7
///
/// **The following information is required for RSA encryption:**
/// - The public key to encrypt with is the base64-encoded ASCII-armored export of the binary key
/// - The data to encrypt with is base64-encoded
/// - PKCS1 encoding is used
class Crypto {
  /// Preventing from initializing this class
  Crypto._();

  static final Random _random = Random.secure();
  static IV getInitVector() => IV.fromSecureRandom(16);

  /// encrypt the data with the given, base64 encoded public key
  static Future<String> rsaEncryptData({required String base64Key, required String data}) async {
    final publicPem = stringToBase64.decode(base64Key);
    final publicKey = RSAKeyParser().parse(publicPem) as RSAPublicKey;
    return _rsaEncrypt(data, publicKey);
  }

  static String _rsaEncrypt(String plaintext, RSAPublicKey publicKey) {
    var cipher = PKCS1Encoding(new RSAEngine());
    cipher.init(true, new PublicKeyParameter<RSAPublicKey>(publicKey));
    var cipherText =
        cipher.process(new Uint8List.fromList(utf8.encode(plaintext)));

    return base64Encode(cipherText);
  }

  /// encrypt the data with the given, base64 encoded public key
  static Future<String> rsaDecryptData({required String base64Key, required String data}) async {
    final publicPem = stringToBase64.decode(base64Key);
    final privateKey = RSAKeyParser().parse(publicPem) as RSAPrivateKey;
    return _rsaDecrypt(base64Decode(data), privateKey);
  }

  static String _rsaDecrypt(Uint8List ciphertext, RSAPrivateKey privateKey) {
    var cipher = PKCS1Encoding(new RSAEngine());
    cipher.init(false, new PrivateKeyParameter<RSAPrivateKey>(privateKey));
    var decrypted = cipher.process(new Uint8List.fromList(ciphertext));

    return new String.fromCharCodes(decrypted);
  }

  static Codec<String, String> stringToBase64 = utf8.fuse(base64);

  /// encrypt the data with the given, base64 encoded public key
  static Future<String> aesEncryptData({required String cipher, required String data}) async {
    final key = Key.fromUtf8(cipher);

    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));

    final iv = getInitVector();

    final encrypted = encrypter.encrypt(data, iv: iv);

    return base64Encode(List.from(iv.bytes)..addAll(encrypted.bytes));
  }

  /// encrypt the data with the given, base64 encoded public key
  static Future<String> aesDecryptData({required String cipher, required String data}) async {
    final key = Key.fromUtf8(cipher);
    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));

    final bytes = base64Decode(data);

    final iv = IV(bytes.sublist(0, 16));

    final encrypted = Encrypted(bytes.sublist(16));

    return encrypter.decrypt(encrypted, iv: iv);
  }

  /// creates a cryptographically random String with the given [length]
  static String createRandomString([int length = 32]) {
    var values = List<int>.generate(length, (i) => _random.nextInt(256));

    return base64Url.encode(values).substring(0, length);
  }

  /// return a [String] with four cryptographically random [int]s inside
  static String createRandomCode() {
    var values = List<int>.generate(4, (i) => _random.nextInt(9));
    return values.join();
  }

  /// returns the base64 encoded SHA256 hash of a given [message]
  static String getSha256Sum(String message) {
    var digest = sha256.convert(message.codeUnits);
    return base64Encode(digest.bytes);
  }

  /// returns the blowfish crypto hash of a given [message]
  static String getBCryptChecksum(String message, String salt) {
    return DBCrypt().hashpw(message, salt);
  }

  /// returns a blowfish crypto salt
  static String getBCryptSalt() {
    return (DBCrypt().gensaltWithRounds(11));
  }

  /// generates a PGP [KeyPair] encrypted with the given passphrase
  static Future<KeyPair> generatePGPKeyPair(String passphrase) async {
    Options options = Options();
    options.email = 'foo@bar.baz';
    options.passphrase = passphrase;
    options.name = 'foo';
    return await OpenPGP.generate(options: options);
  }

  /// encrypts the given [data] with a base64 encoded PGP public key
  static Future<String> pgpEncryptData({required String base64Key, required String data}) async {
    final publicKey = stringToBase64.decode(base64Key);
    return await OpenPGP.encrypt(data, publicKey);
  }

  /// generates a public-private RSA keypair
  static Future<RSAKeyPair> generateRSAKeyPair({int bitStrength = 2048}) async {
    // using the friendly helpers by `rsa_encrypt` saving us
    // dozens of lines of code
    var helper = RsaKeyHelper();
    final keys = await helper.computeRSAKeyPair(
      helper.getSecureRandom(),
      bitStrength: bitStrength,
    );

    return RSAKeyPair(
      stringToBase64.encode(helper.encodePrivateKeyToPemPKCS1(keys.privateKey as RSAPrivateKey)),
      stringToBase64.encode(helper.encodePublicKeyToPemPKCS1(keys.publicKey as RSAPublicKey)),
    );
  }
}
